function getIP(callback) {
  $.get('https://api.ipify.org', function (data) {
      return callback(undefined, data.toString().trim());
    })
    .fail(function (err) {
      return callback(true, null);
    });
};

getIP(function (err, ip) {
  $('.loading').hide();
  if (err) {
    $('.failed').show();
  } else {
    $('.ip-field').val(ip);
    $('.ip').show();
    $('.copy-btn').click(function () {
      copy(ip);
    });
  }
});

function copy(text) {
  chrome.runtime.sendMessage({
    type: 'copy',
    text: text
  });
  $('.copy-btn').html('<i class="fa fa-check"></i>');
  $('.copy-btn').prop('disabled', true);
};
